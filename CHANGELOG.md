<!--
 * @Author: wangxiaoheng
 * @Date: 2022-08-30 14:36:04
 * @LastEditTime: 2022-08-30 17:03:32
 * @LastEditors: your name
 * @Description: 
 * @FilePath: \npm-buju\CHANGELOG.md
 * 可以输入预定的版权声明、个性签名、空行等
 * Copyright (c) 2022 by wangxiaoheng, All Rights Reserved. 
-->
### 1.0.2 (2021-08-30)

* 修改介绍文档添加使用说明和包安装位置
### 1.0.1 (2021-08-30)

* 修改介绍文档
### 1.0.0 (2021-08-30)

* 添加项目
* 添加文档
* 添加代码注释