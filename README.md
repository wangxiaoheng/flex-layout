
                         _oo0oo_
                        o8888888o
                        88" . "88
                        (| -_- |)
                        0\  =  /0
                      ___/`---'\___
                    .' \\|     |// '.
                   / \\|||  :  |||// \
                  / _||||| -:- |||||- \
                 |   | \\\  - /// |   |
                 | \_|  ''\---/''  |_/ |
                 \  .-\__  '-'  ___/-. /
               ___'. .'  /--.--\  `. .'___
            ."" '<  `.___\_<|>_/___.' >' "".
           | | :  `- \`.;`\ _ /`;.`/ - ` : | |
           \  \ `_.   \_ __\ /__ _/   .-` /  /
       =====`-.____`.___ \_____/___.-`___.-'=====
                         `=---='
  
  
       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  
             佛祖保佑     永不宕机     永无BUG
  
         佛曰:  
                 写字楼里写字间，写字间里程序员；  
                 程序人员写程序，又拿程序换酒钱。  
                 酒醒只在网上坐，酒醉还来网下眠；  
                 酒醉酒醒日复日，网上网下年复年。  
                 但愿老死电脑间，不愿鞠躬老板前；  
                 奔驰宝马贵者趣，公交自行程序员。  
                 别人笑我忒疯癫，我笑自己命太贱；  
                 不见满街漂亮妹，哪个归得程序员？


<a href="https://gitee.com/wangxiaoheng/flex-layout">
  <!-- <img src="" alt="Normalize Logo" width="80" height="80" align="right"> -->
  库地址
</a>
> 本项目会持续更新

> This is a flex layout scheme

 **NPM**

```sh
npm i css-layout-flex
```

```sh
使用:在vue项目中的 main.js import 'css-layout-flex'  直接引入
```

### 项目介绍
├── @/node_modules/css-layout-flex  # 项目安装目录  
│  
├── overallCenter              # 居中布局  
│  
├── overallLeft                # 居中起点对齐  
│  
├── overallRight               # 居中终点对齐  
│  
├── overallAround              # 项目两边之间的空白间距是相等的，垂直居中  
│  
├── overallbaseline            # 居中两端对齐，项目之间的间隔都是相等的 对齐  
│  
├── overallBetween             # 项目均匀对齐，换行  
│  
├── overallWrap-Left           # 项目起点对齐，换行  
│  
├── overallWrap-content        # 项目居中，换行  
│  
├── overallBetweenWrap         # 居中两端对齐，项目之间的间隔都是相等的 可换行  
│  
├── overallVerticalLevelCentered         # 多个元素垂直方向水平居中  
│  
├── overallVerticalCentered    # 垂直方向居中对齐  
│  
├── omissionOfWords            # 多行文字省略  
│  
├── singleline                 # 单行文字省略  
│  
├── noCopying                  # 禁止复制文字  
│  
├── AlignTop                   # 顶部对齐  
│  
└── TEXTdistribution           # 文字分布  注：必须是文字，如果是数字或者abc这种要用空格分开，否则连一起。  